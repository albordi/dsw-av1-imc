const express = require('express');
const port = 5000;

const app = express();
app.set('view engine', 'ejs');
app.set('views', './app/views');
app.use(express.static('./public'));

app.listen(port, function () {
	console.log('Servidor rodando com express na porta', port);
});

module.exports = app;